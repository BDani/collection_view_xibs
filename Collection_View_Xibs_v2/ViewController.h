//
//  SecondViewController.h
//  Nav_Bar_Table_View_ObjC_v2
//
//  Created by Bruno Tavares on 20/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *exitButton;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
