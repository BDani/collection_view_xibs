//
//  SecondViewController.m
//  Nav_Bar_Table_View_ObjC_v2
//
//  Created by Bruno Tavares on 20/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"

NSUInteger const kLabelHeight = 300;
NSUInteger const kLabelWidth = 70;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (IBAction)pressExitButton:(id)sender {
    
    self.view.hidden = true;
}
@end
