//
//  CollectionViewCell.m
//  Collection_View_Xibs_v2
//
//  Created by Bruno Tavares on 26/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "CollectionViewCell.h"

@interface CollectionViewCell ()

@property (nonatomic, strong) NSString *photoString;

@end



@implementation CollectionViewCell

-(void) setPhoto: (NSString *) photoString {

    self.photoString = photoString;

    self.imageView.image = [UIImage imageNamed:self.photoString];
}

@end
