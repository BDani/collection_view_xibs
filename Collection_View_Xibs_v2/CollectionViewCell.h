//
//  CollectionViewCell.h
//  Collection_View_Xibs_v2
//
//  Created by Bruno Tavares on 26/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

-(void) setPhoto: (NSString *) photoString;

@end
